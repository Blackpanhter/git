package com.example.myapplication

import android.os.Bundle
import android.os.StrictMode
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import com.example.myapplication.databinding.ActivityMainBinding
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import okhttp3.OkHttpClient
import okhttp3.Request


class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)

        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)


        val navController = findNavController(R.id.nav_host_fragment_content_main)
        appBarConfiguration = AppBarConfiguration(navController.graph)
        setupActionBarWithNavController(navController, appBarConfiguration)

        binding.fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }

        val client = OkHttpClient()

        val request = Request.Builder()
            .url("https://community-open-weather-map.p.rapidapi.com/weather?q=London%2Cuk&lat=0&lon=0&id=2172797&lang=null&units=metric&mode=json")
            .get()
            .addHeader("X-RapidAPI-Host", "community-open-weather-map.p.rapidapi.com")
            .addHeader("X-RapidAPI-Key", "9d88a41e15msh3fd611d9a98c651p14e59bjsn81806d03a3ad")
            .build()

        //val response = client.newCall(request).execute()
        //setContentView(R.layout.fragment_first)
        val myAwesomeTextView = findViewById(R.id.textView2) as TextView
        val gson: Gson = Gson()
        //val resultt: String = Gson().toJson(response.body!!.string())
        val responseBody = client.newCall(request).execute().body
        val test: String = "{\"coord\":{\"lon\":-0.1257,\"lat\":51.5085},\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"overcast clouds\",\"icon\":\"04d\"}],\"base\":\"stations\",\"main\":{\"temp\":17.67,\"feelslike\":16.46,\"tempmin\":15.95,\"temp_max\":19.44,\"pressure\":1016,\"humidity\":37},\"visibility\":10000,\"wind\":{\"speed\":9.26,\"deg\":90},\"clouds\":{\"all\":90},\"dt\":1650457378,\"sys\":{\"type\":2,\"id\":268730,\"country\":\"GB\",\"sunrise\":1650430457,\"sunset\":1650481452},\"timezone\":3600,\"id\":2643743,\"name\":\"London\",\"cod\":200}"
        var result: Weatherr = gson.fromJson(test, Weatherr::class.java)
        myAwesomeTextView.text = result.name + " " + result.main.temp


    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration)
                || super.onSupportNavigateUp()
    }
}