package com.example.myapplication

class Weatherr (
    val coord: Coord,
    //val weather: List<Weather>,
    val base: String,
    val main: Main,
    val visibility: Long,
    val wind: Wind,
    val clouds: Clouds,
    val dt: Long,
    val sys: Sys,
    val timezone: Long,
    val id: Long,
    val name: String,
    val cod: Long
) {
}

class Clouds (
    val all: Long
)

class Coord (
    val lon: Double,
    val lat: Double
)

class Main (
    val temp: Double,
    val feelslike: Double,
    val tempmin: Double,
    val tempMax: Double,

    val pressure: Long,
    val humidity: Long
)

class Sys (
    val type: Long,
    val id: Long,
    val country: String,
    val sunrise: Long,
    val sunset: Long
)

class Weather (
    val id: Long,
    val main: String,
    val description: String,
    val icon: String
)

data class Wind (
    val speed: Double,
    val deg: Long
)